
<!-- _Use this merge request description template for adding new third-party apps to the
[GitLab Applications](https://about.gitlab.com/applications/) page._ -->

For the contribution process and guidelines, please check
[Integrate with GitLab](https://about.gitlab.com/partners/integrate/#integrating-with-gitlab).

Checklist:

1. [ ] Application's data added to [`data/applications.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/applications.yml)
1. [ ] Application's logo added to [`source/images/applications/apps/`](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/images/applications/apps) (optional, but recommended)
1. [ ] Eliran's review - cc/ @eliran.mesika
1. [ ] Tech writer's review
