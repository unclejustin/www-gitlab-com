---
layout: markdown_page
title: "Product Vision - Mobile"
---

- TOC
{:toc}

Developing and delivering mobile apps with GitLab is a critical capability. Many
technology companies are now managing a fleet of mobile applications, and being
able to effectively build, package, test, and deploy this code in an efficient,
effective way is a competitive advantage that cannot be understated. GitLab is
taking improvements in this area seriously, with a unified vision across several
of our [DevOps stages](/direction/#devops-stages).

To see an example of where we're headed and the kinds of easy-to-use flows we
have in mind, check out
[this video](https://www.youtube.com/watch?v=325FyJt7ZG8) where we demonstrate
making a change to a mobile app using an iPad pro, send the build over to a
MacOS runner, and receive a working copy back to test on the very same device
using TestFlight. You can also see how to get up and running with Android builds
and publishing today in our
[recent blog post](/2019/01/28/android-publishing-with-gitlab-and-fastlane/)
(look for the iOS version coming shortly.)

## North Stars

We apply our global product strategy to thinking about Mobile users. We treat the following
principles as our north stars:
- **Single Developer Application** - GitLab will expand in breadth to be your mobile development hub.   
- **Concurrent DevOps** - All users throughout the mobile development lifecycle, whether they are in 
  QA, design, product management, security, release or support, will be able to contribute.
- **Lovable Experience** - Mobile developers deserve a tool that is friendly, slick, and makes them look like heroes.

## Stages with mobile focus

There are several stages involved in delivering a comprehensive, quality mobile experience at GitLab. These include, but are not necessarily limited to the following (with examples of what each area might deliver):

- [Manage](/direction/manage): Offering a shared MacOS runners fleet for doing iOS builds, comprehensive templates to get started quickly.
- [Create](/direction/create): Web IDE features that allow you to easily manage the kinds of code and artifacts you work with during mobile development.
- [Verify](/direction/verify): Runners for MacOS, Linux-based builds for iOS.
- [Package](/direction/package): Build archives for mobile applications.
- [Release](/direction/release): Review apps for mobile development, code signing and publishing workflows to TestFlight or other distribution models.

## Highlighted epics and issues

There are a few epics and important issues you can check out to see where we're headed.

- [&769](https://gitlab.com/groups/gitlab-org/-/epics/769): Improve mobile CI/CD in GitLab
- [gitlab-ce#57882](https://gitlab.com/gitlab-org/gitlab-ce/issues/57882): New project templates for iOS and Android
- [www-gitlab-com#3592](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3592): Blog post for iOS development using FastLane
