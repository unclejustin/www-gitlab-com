---
layout: markdown_page
title: "Category Vision - Compatibility Testing"
---

- TOC
{:toc}

## Compatibility Testing

Compatibility testing is a broad discipline and includes things such as hardware testing for software that runs on different devices, as well as multi-cloud compatibility testing which is becoming more and more important in a cloud-based world where you don't want all your eggs in one basket.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=compatibility%20testing&sort=milestone)
- [Overall Vision](https://about.gitlab.com/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

## What's Next & Why

Next up for compatibility testing is [gitlab-ee#6061](https://gitlab.com/gitlab-org/gitlab-ee/issues/6061), which will introduce Selenium integration to capture multi-browser testing results as well as screenshots and display them in a CI view. This will start the ball rolling forward on how we can better support these kinds of testing needs.

## Competitive Landscape

No other CI platforms provide first-party compatibility testing, but all do provide different kinds of integrations. Selenium is a very popular one, and we plan to add a CI view for it via [gitlab-ee#6061](https://gitlab.com/gitlab-org/gitlab-ee/issues/6061).

## Analyst Landscape

We do not engage with analysts for compatibility testing market analysis.

## Top Customer Success/Sales Issue(s)

None reported.

## Top Customer Issue(s)

None reported.

## Top Internal Customer Issue(s)

Apart from the CI view support for Selenium mentioned in the competitive landscape section, [gitlab-ce#22755](https://gitlab.com/gitlab-org/gitlab-ce/issues/22755) (integration support for SauceLabs) is also requested. [team-tasks#45](https://gitlab.com/gitlab-org/quality/team-tasks/issues/45) tracks the progress of the internal quality team at GitLab rolling out compatibility testing.

## Top Vision Item(s)

Adding CI view support for Selenium ([gitlab-ee#6061](https://gitlab.com/gitlab-org/gitlab-ee/issues/6061)) is the most important vision item for the same reasons as in the competitive landscape above.