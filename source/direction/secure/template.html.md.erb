---
layout: markdown_page
title: Product Vision - Secure
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Overview

The Secure stage aims to ensure that applications developed with GitLab are not
affected by vulnerabilities that may lead to security problems and unintended
use. This can be achieved by covering the Dev side of the DevOps lifecycle with features
that allow both security teams and developers to know if there is something that
they should consider in their apps, before it is too late to create a fix.

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/caZXzNCgI9k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

- See a high-level
  [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Asecure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).

## Security Paradigm

GitLab security features support users in prioritizing, managing, and solving
any security issue that may affect their environment. The primary focus is to
increase awareness on security (onboarding) and to provide all the information
needed to take decisions about that.

The approach is to _support_ decision makers, not to replace them. Instead of
enforcing security, we want to give a very simple way to take the right action,
and learn from it. Keeping it simple is a key value to prevent security
features from being considered at all because they require more effort than the
perceived benefit.

That's why security features are not supposed to automatically block a pipeline
nor prevent a new version from being released to production. Even accurate results may be considered false positives as this is somewhat subjective by nature. Risk assessment
will be mostly a human process.

Tools are actionable: it means that users can
[interact with them and provide feedback](https://docs.gitlab.com/ee/user/project/merge_requests/#interacting-with-security-reports)
about their content. When triaging vulnerabilities, users can confirm
(creating an issue to solve the problem), or dismiss them (in case they are
false positives and there is no further action to take). This information will be
collected to improve the signal-to-noise ratio that the tool could provide in
future executions.

They also need to be easy to use, and require the minimum amount of effort from
users: We don't want to add extra workload on final users.
Otherwise, security checks will likely be disabled or not considered at all,
missing their primary goal. Imagine if users had to explain why they are marking
an email as spam every time!

<%= partial("direction/categories", :locals => { :stageKey => "secure" }) %>

## Other top-level features

In the Secure stage there are features that are cross-category. They allow Security
Teams and Developers to manage security with a holistic approach.
The prioritization of a security issue doesn't fully depend on the type of
vulnerability, but on its severity and so which is the impact on the application.

That's why we want to create features where vulnerabilities are in one single place,
no matter if they are coming from SAST, Dependency Scanning, Container Scanning, etc.

### Auto Remediation

When a vulnerability is automatically detected by GitLab, users can be aware
of that using the Security Dashboard or looking at security reports. But this
still requires manual intervention to create a fix and push it to production,
and during this time there is a vulnerable window where attackers can leverage
the vulnerability.

Auto Remediation aims to automate vulnerability solution flow, and automatically create
a fix. The fix is then tested, and if it passes all the tests already defined
for the application, it is deployed to production.

GitLab can then monitor performances of the deployed app, and revert all the
changes in case performances are decreasing dramatically, warning the user
about the entire process and reducing the need for manual actions.

Read more in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/759).

<figure class="video_container">
   <iframe width="560" height="315" src="https://www.youtube.com/embed/ZgFqyXCsqPY?start=4216" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

Slides are available [here](https://docs.google.com/presentation/d/1VuUKRnMlISEK4ECPaJVOYv3Pom4Zh6JXBFbD58RuibI/edit#slide=id.g4286011fc8_8_0).

### Security Dashboards

Security Dashboards, available at [group](https://docs.gitlab.com/ee/user/group/security_dashboard/)
and [project](https://docs.gitlab.com/ee/user/project/security_dashboard.html) level,
are the primary tool for Security Teams and Directors of Security. They can use those
dashboards to access the current security status of their applications, and to start a
remediation process from here.

The dashboard is also giving stats and charts to figure out how the team is
performing to keep the security level to a proper level.

### Bill of materials

Software is often based on many components that are reused. Every moderm programming
language makes easy to pull and use external libraries via package managers.
There is a growing need to know exactly what is included in the final app, and the
relevant information about those third-party components, like the version number,
the license, and the security status.

The bill of materials (BOM) makes this information available and accessible, so
compliance can perform validation that the app can be released and deployed.

Read more in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/858).

## DevOps coverage

The classic DevOps includes security testing as an activity in the **Verify**
stage.

GitLab has a broader approach. That's why we introduced a **Secure** stage that
is transversal to all other **Dev** stages.

Our Product Vision aims to increase the security coverage for all these stages
in the [DevOps lifecycle](https://en.wikipedia.org/wiki/DevOps_toolchain):
**Plan**, **Create**, **Verify**, and **Package**.

This is possible because GitLab is a
[single application](https://about.gitlab.com/handbook/product/single-application/).

There are many advantages coming from complete coverage of all these stages with
security features. It allows to shift-left security at a very early stage, even
before the first lines of code are committed. If you start with security as a
priority when you plan a new app, it is easier than just trying to add security
later. Security should remain on the radar even when the create process happens,
to support developers and help them to write secure code from the beginning.
Security testing can then happen for any change, before it is merged into the
stable branch of the application: in this way you can spot problems and fix them
before they risk affecting the production environment or the stable version of
your app. Packaging the app should also be done with security in mind, as well
as the release process to the production environment: at that point, your code
is publicly available and any remediation will impact heavily on your processes.

Security is a never-ending priority, and at GitLab we want to make it easy for
our users to manage it.

## Target audience

### Security teams

We want to support security teams as first class citizens. GitLab should be
their primary tool to manage monitoring and remediation of security issues.
Using the **Security Dashboard** security specialists know exactly which is the
most important thing they need to take care of, while Directors of Security can
manage workflows and analyze historical data to figure out how to improve the
response time.

This is a vulnerability-centric approach where items are grouped and ordered to
suggest what's most important in a group, or in the entire instance.

### Developers

Nonetheless, we want to support developers and provide feedback during the
application development.
[**Security Reports**](https://docs.gitlab.com/ee/user/project/merge_requests/#security-reports)
in merge request widgets and pipelines allow early access to security
information that can be used to fix problems even before they are merged into
the stable branch or released to the public, embracing the idea of [shift left testing](https://en.wikipedia.org/wiki/Shift_left_testing).

This approach is valuable to highlight how specific changes could affect the
security of the application.

## Impact

We expect that in most of the cases the number of potential security issues will
be high, and we don't want that users will struggle in figuring out which is the
impact of any possible item for their environment. That's why GitLab will
prioritize security vulnerabilities based on different factors. The value given
with this process is defined as the impact.

These are examples of factors that may contribute to define the impact for a
specific security issue:
- severity and confidence levels, provided by the analysis tool
- feedback given in other reports for the same vulnerability
- exposure of the vulnerability (e.g., app has already been deployed to production)

## Availability

### Ultimate/Gold subscribers

For now, security features are available in the [Ultimate/Gold tier](https://about.gitlab.com/pricing/)
because we think that security matters for everyone, but automation of security
in the development lifecycle is more valuable for organizations who want to
optimize their DevOps investment to run securely at the highest business
velocity.

### Public projects on GitLab.com

Every project on [GitLab.com](https://gitlab.com) with
[public visibility](https://docs.gitlab.com/ee/public_access/public_access.html#public-projects)
can benefit of all the security features for free, even if it doesn't have a Gold license.

Nevertheless, public groups can benefit of group security features (like the
[Group Security Dashboard](https://docs.gitlab.com/ee/user/group/security_dashboard/)
only if they have a Gold license.

## Metrics

We also want to collect and provide metrics to better understand how the
security workflow is performing. For example, the overall time taken to deploy a
fix for a vulnerability once spotted could be useful for flow improvements.

## Prioritization Process

Every month, a couple of weeks before the start of the development cycle, the
team meets to plan what will be done in the upcoming milestone. Decisions are
based on our general
[prioritization principles](https://about.gitlab.com/handbook/product/#prioritization),
and we specifically consider case by case what is most important to do next,
since we always have so many awesome ideas but we have to choose what has to be
done first.

The PM maintains a [product roadmap](https://gitlab.com/groups/gitlab-org/-/boards/767789)
for the next three milestones and beyond, to ensure that we are focusing on
important things ahead of time and we can prepare them properly. We are always
ready to adjust our plans and we normally commit only to issues that are
scheduled for the current release: this is very important in order to guarantee
that we can be fast enough to react to unexpected events.

We want everybody to be part of our prioritization process, that's why all the
information is public. We encourage team members, customer, and the community to
give feedback and to share what they consider very important to focus on in the
future. This is a valuable help to make GitLab a great product.

## Upcoming Releases

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "secure" }) %>
