---
layout: job_family_page
title: "Handbook Content Manager"
---

The Web Content Manager for the [GitLab Handbook](/handbook/) is a key member of the GitLab Team. The primary functions are to manage design and content for GitLab’s Handbook, as well as to create, maintain, update, edit, and curate all website content for our handbook. Our handbook isn’t your typical handbook and is a strategic, fluid site that is critical for our success. It is viewable both internally and externally to GitLabbers, investors, customers, candidates, and everyone else around the world. The position will be a liaison between People Ops and internal stakeholders with website content and will integrate content from multiple teams. The Web Content Manager will be dedicated to exceptional user experience, ensuring that the web experience is relevant, concise, intuitive, and visually compelling, with content that encourages engagement and return.

Individuals applying for this position must be self-starters, flexible in changing direction and methods, thrive in a fast paced environment, excel at managing multiple priorities, and have the prior work experience needed to quickly learn our platform.

## Key Responsibilities

* Ensure web content is fresh, fully developed, expertly edited, and search engine optimized; it must also contain high quality supporting elements such as images and videos as needed.
* Ensure new pages are user friendly and link properly with existing pages, eliminating duplicate content within the site. Ensure viewers can navigate easily and be linked seamlessly with other relevant web pages.
* Coordinate and oversee the editorial and quality assurance process with content creators.
* Work with publishers to resolve technical issues related to the proper display of content and to develop automated website features.
* Optimize web content for end user performance. Work may include formatting text, assuring hyperlink integrity, transferring and translating files, and posting of new material to the page.
* Maintain web site content and information flow.
* Find, diagnose, and fix content problems, including broken links, typographical errors, and formatting inconsistencies. Perform website administration, such as setting provisions and managing access and workflows.
* Ensure all published content complies with corporate information security policies, corporate PR standards, established usability standards, copyright laws, and applicable regulations.
* Work with stakeholders to maintain existing content and develop new content to promote programs and services.
* Assist in the maintenance of security processes and procedures to ensure site security.
* Compile, analyze, and report statistics regarding all relevant website matrix.

## Required Skills and Background

* Bachelor's degree or equivalent work experience.
* 2+ years' experience in the High Tech industry preferred.
* 3-5 years' experience in web content management.
* HTML, CSS skills that allow you to make a great looking jobs page.
* Javascript proficiency, for example build a compensation calculator.
- Single page application, for example a team page with quick selectors
- Advanced git knowlege, fix a complex merge that went sideways for someone.
- Experience making tests, for example only allowing relative urls.
- Web server knowledge, for example programmatic redirects of webpages.
- Programming experience, for example parsing a yaml file and generating multiple pages with it.
- Programming skills that allow you to automatically post job-families to vacancies.
- Speeding up site builds knowlege, for example by caching results.
- Static site generation knowlege, for example convert the website to different static site generator.
* Ability to clearly describe and troubleshoot issues.
* Clear understanding of web standards and usability methods.
* Experience with CMS (Content Management Systems).
* Knowledge of multimedia and design.
* Strong writing and editing skills.
* Ability to prioritize and balance workload to meet aggressive deadlines.
* Ability to work with colleagues at all levels of the organization.
* Knowledge of communications, production, terminology, strategies.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters.
* Next, candidates will be asked to complete a take home technical challenge. 
* Next, candidates will be invited for a 45 minute interview with one of our Frontend Developers.
* Candidates will be invited to schedule a 45 minute with our People Operations Analyst.
* Candidates may be asked to schedule a 50 minute interview with our Director, People Operations. 
* Finally, candidates may be asked to schedule a 50 minute interview with our CEO.
* Successful candidates will subsequently be made an offer via email.

See more details about our hiring process on the [hiring handbook](/handbook/hiring).
