---
layout: job_family_page
title: "Fullstack Engineers"
---

Fullstack Engineers at GitLab work in the Growth department, which analyzes the entire customer journey from acquisition of a customer, to the flow across multiple GitLab features, and even reactivation of lost users.  They work in small groups with a product manager, UX designer and a data analyst in order to scale GitLab usage by connecting users to the existing value that GitLab already delivers.

## Responsiblities

* Develop features and improvements to the GitLab product in a secure, well-tested, and performant way
* Analyze and interpret highly complex data to arrive at actionable recommendations
* Collaborate with Product Management and other stakeholders within Engineering (e.g. UX) to maintain a high bar for quality in a fast-paced, iterative environment
* Advocate for improvements to product quality, security, and performance
* Solve technical problems of moderate scope and complexity. 
* Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
* Represent GitLab and its values in public communication around specific projects and community contributions. 
* Confidently ship small features and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.

## Requirements

* Professional experience with Ruby and Rails
* Professional experience with JavaScript and associated web technologies (CSS, semantic HTML)
* Proficiency in the English language, both written and verbal, sufficient for success in a remote and largely asynchronous work environment
* Demonstrated capacity to clearly and concisely communicate about complex technical, architectural, and/or organizational problems and propose thorough iterative solutions
* Experience with performance and optimization problems and a demonstrated ability to both diagnose and prevent these problems
* Comfort working in a highly agile, [intensely iterative][iteration] software development process
* Demonstrated ability to onboard and integrate with an organization long-term
* Positive and solution-oriented mindset
* Effective communication skills: [Regularly achieve consensus with peers][collaboration], and clear status updates
* An inclination towards communication, inclusion, and visibility
* Experience owning a project from concept to production, including proposal, discussion, and execution.
* [Self-motivated and self-managing][efficiency], with strong organizational skills.
* Demonstrated ability to work closely with other parts of the organization
* Share [our values][values], and work in accordance with those values
* Ability to thrive in a fully remote organization

[values]: /handbook/values/
[collaboration]: /handbook/values/#collaboration
[efficiency]: /handbook/values/#efficiency
[iteration]: /handbook/values/#iteration

#### Nice-to-haves

* Experience working with modern frontend frameworks (eg. React, Vue.js, Angular)
* Experience in a peak performance organization, preferably a tech startup
* Experience with the GitLab product as a user or contributor
* Product company experience
* Experience working with a remote team
* Enterprise software company experience
* Developer platform/tool industry experience
* Experience working with a global or otherwise multicultural team
* Computer science education or equivalent experience
* Passionate about/experienced with open source and developer tools

## Relevant links

- [Product Vision - Growth](/direction/growth/)
- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to complete a short written assessment.
* Next, candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a 60 minute technical and behavioral interview with an Engineering Hiring Manager
* Candidates will then be invited to schedule a 60 minute interview with the Director of Engineering
* Successful candidates will subsequently be made an offer.

Additional details about our process can be found on our [hiring page](/handbook/hiring).