---
layout: markdown_page
title: "Periscope Directory"
description: "GitLab Periscope Directory"
---

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

{::options parse_block_html="true" /}

----

Request Access to Periscope with an [Access Request Issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)

## Periscope Resources
* **[Periscope Data Onboarding: Creating and Analyzing Charts/Dashboards](https://www.youtube.com/watch?v=F4FwRcKb95w&feature=youtu.be)**
* **[Getting Started With Periscope Data!](https://doc.periscopedata.com/article/getting-started)**
* [Periscope Guides](https://www.periscopedata.com/resources#guides)
* [Periscope Community](https://community.periscopedata.com)
* [Documentation](https://doc.periscopedata.com)

## Spaces

We have two Periscope [spaces](https://doc.periscopedata.com/article/spaces#article-title):
* GitLab
* GitLab Sensitive

They connect to the data warehouse with different users- `periscope` and `periscope_sensitive` respectively. 

Most work is present in the GitLab space, though some _extremely sensitive analyses_ will be limited to GitLab sensitive. Examples of this may include analyses involving contractor and employee compensation and unanonymized interviewing data. 

Spaces are organized with tags. Tags should map to function (Product, Marketing, Sales, etc) and subfunction (Create, Secure, Field Marketing, EMEA). Tags should loosely match [issue labels](handbook/business-ops/data-team/#issue-labeling) (no prioritization). 
Tags are free. Make it as easy as possible for people to find the information they're looking for. At this time, tags cannot be deleted or renamed. 

* [Example Dashboard](https://app.periscopedata.com/app/gitlab/403199/Example-Dashboard)

### GitLab Space

* [Corporate Metrics](https://app.periscopedata.com/app/gitlab/409920/Corporate-Metrics)
* [Distribution of Customers to ARR](https://app.periscopedata.com/app/gitlab/416459/Distribution-of-Customers-to-ARR)
   * [Customers by ARR Rank](https://app.periscopedata.com/app/gitlab/416590/Customers-by-ARR-Rank)
* [GitLab.com Customer Retention](https://app.periscopedata.com/app/gitlab/412223/GitLab.com-Customer-Retention)
* [GitLab.com Sandbox](https://app.periscopedata.com/app/gitlab/406359/Gitlab.com-Sandbox)
* [Month over Month SMAU](https://app.periscopedata.com/app/gitlab/425984/WIP:-Month-over-Month-SMAU)
* [New Business Sales Cycle - Trailing 12 Months](https://app.periscopedata.com/app/gitlab/408541/New-Business-Sales-Cycle---Trailing-12-Months)
* [Retention](https://app.periscopedata.com/app/gitlab/403244/Retention)
   * [ARR by Fiscal year Cohort](https://app.periscopedata.com/app/gitlab/422746/ARR-by-Fiscal-Year-Cohort)
   * [Subscription Information and MRR](https://app.periscopedata.com/app/gitlab/422776/Subscription-Information-and-MRR)
   * [Parent Account Retention by Size and Segment](https://app.periscopedata.com/app/gitlab/422779/Parent-Account-Retention-by-Size-and-Segment)
* [Sales](https://app.periscopedata.com/app/gitlab/423607/Sales-(Formerly-Boardroom-TV))
   * [Won Opportunities Drill Down](https://app.periscopedata.com/app/gitlab/425606/Won-Opportunity-Drill-Down)
   * [SAO Opportunity Drill Down](https://app.periscopedata.com/app/gitlab/425981/SAO-Opportunity-Drill-Down)
   * [SQO Opportunity Drill Down](https://app.periscopedata.com/app/gitlab/425982/SQO-Opportunity-Drill-Down)
* [Sales Forecast](https://app.periscopedata.com/app/gitlab/408411/Sales-Forecast)
* [Secure Metrics](https://app.periscopedata.com/app/gitlab/410654/Secure-Metrics)
* [Snowplow Summary Metrics](https://app.periscopedata.com/app/gitlab/417669/Snowplow-Summary-Dashboard)
* [Version Upgrade Rate](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate)

#### Meta Metrics for the Data Team

* [Periscope Usage! 📈](https://app.periscopedata.com/app/gitlab/410320/Periscope-Usage!-📈)
* [Periscope Account Optimization 💪](https://app.periscopedata.com/app/gitlab/410321/Periscope-Account-Optimization-💪)
* [Periscope Account Maintenance 🗑️](https://app.periscopedata.com/app/gitlab/410322/Periscope-Account-Maintenance-🗑%EF%B8%8F)
* [dbt Event Logging](https://app.periscopedata.com/app/gitlab/420622/dbt-Event-Logging)

### GitLab Sensitive Space

This space is not used at this time. 

## User Roles

There are three user roles (Access Levels) in Periscope: admin, SQL, and View Only. 

The current status of Periscope licenses can be found in [the analytics project](https://gitlab.com/gitlab-data/analytics/blob/master/analyze/periscope_users.yml).

### Administrators 

These users have the ability to provision new users, change permissions, and edit database connections. (Typical admin things)

Resource: [Onboarding Admins](https://www.youtube.com/watch?v=e-cZgf6zzlQ&feature=youtu.be)

### Editor access

The users have the ability to write SQL queries against the `analytics` schema of the `analytics` database that underlie charts and dashboards. They can also create or utilize SQL snippets to make this easier. **There are a limited number of SQL access licenses, so at this time we aim to limit teams to one per Director-led team. It will be up to the Director to decide on the best candidate on her/his team to have SQL access.**

### View only users

These users can consume all existing dashboards. They can change filters on dashboards. Finally, they can take advantage of the [Drill Down](https://doc.periscopedata.com/article/drilldowns) functionality to dig into dashboards. 

### Notes for when provisioning users

Make an MR to the analytics repo updating the permissions file and link it in your provisioning message. This helps affirm who got access to what, when, and at what tier. 

In the Periscope UI, navigate to the **Directory** (not the Settings. This is important since we have Spaces enabled) to add the new user using her/his first and last names and email. Then add the user to the "All Users" group  and their function group (e.g. Marketing, Product, etc.) by clicking the pencil icon on the right side of the page next to "Group". If it is an editor user, then add her/him to the "Editor" group. 

Users will inherit the highest access from any group they are in. This is why all functions are by default View only.

Permissions for a group are maintained under the space "Settings" section. (This is very confusing.) To upgrade or downgrade a group, you need to do that under setting, not under the Directory. 

## Data Team Workflow

1. Issue in [Analytics Project](https://gitlab.com/gitlab-data/analytics/) 
2. Write SQL queries in Periscope; Dashboard should be named `WIP: Dashboard Name`
3. Add the hyperlinked dashboard to this directory (with just the `Dashboard Name`, no WIP)
4. Deliver dashboard in original issue (See [Data Analysis Process](/handbook/business-ops/data-team/#-data-analysis-process) for more details)


