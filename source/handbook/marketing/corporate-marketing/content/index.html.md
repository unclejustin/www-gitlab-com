---
layout: markdown_page
title: "Content Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Content Marketing

The Content Marketing team includes audience development, editorial oversight,
social marketing, and content strategy, development, and
operations. The Content Marketing team is responsible for the stewardship of
GitLab's audiences, users, customers, and partners' content needs, preferences and
perceptions of GitLab. Content marketing creates engaging, inspiring, and
relevant content, executing integrated content programs to deliver useful and
cohesive content experiences that build trust and preference for GitLab.

## Other related pages

- [Social marketing handbook](/corporate-marketing/social-marketing/)
- [GitLab blog](/handbook/marketing/blog)
- [Blog calendar](/handbook/marketing/blog/#blog-calendar)
- [Editorial review checklist](/handbook/marketing/corporate-marketing/content/editorial-review-checklist/)
- [Social media guidelines](/handbook/marketing/social-media-guidelines/)
- [Content Hack Day](/handbook/marketing/corporate-marketing/content/content-hack-day)
- [Newsletters ](/handbook/marketing/marketing-sales-development/marketing-programs/#newsletter) (managed by Marketing Programs)
- [User spotlights](/handbook/marketing/corporate-marketing/content/user-spotlights)

## Communication

### Chat
Please use the following Slack channels:

- `#content` for general inquiries
- `#content-updates` for updates and log of new, published content
- `#blog` for questions regarding blog content
- `#twitter` for questions about social
- `#content-hack-day` for updates and information on Content Hack Day

### Issue trackers
  - [Content Marketing](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/911769?&label_name[]=Content%20Marketing)
  - [Blog posts](https://gitlab.com/gitlab-com/www-gitlab-com/boards/804552?&label_name[]=blog%20post)

### Team
- **Erica Lindberg, Manager, Content Marketing**
  - Contact: [@erica](https://gitlab.com/erica)
- **Rebecca Dodd, Managing Editor**
  - Contact: [@rebecca](https://gitlab.com/rebecca)
- **Emily von Hoffmann, Associate Social Marketing Manager**
  - Contact: [@evhoffmann](https://gitlab.com/evhoffmann)
- **Aricka Flowers, Digital Production Manager**
  - Contact: [@atflowers](https://gitlab.com/atflowers)
- **Suri Patel, Content Marketing Associate, Dev**
  - Contact: [@spatel](https://gitlab.com/suripatel)

## Editorial Mission Statement

Empower and inspire software teams to adopt and evolve a DevOps workflow to collaborate better, be more productive, and ship faster by sharing insightful and actionable information, advice, and resources.

### Vision

Build the largest and most diverse community of cutting edge co-conspirators who are leading the way to define and create the next generation of software development practices.

### 2018 Core content strategy statement

The content we produce helps increase awareness of GitLab’s complete and single application with the goal of broadening our market share and increasing sales by providing informative and persuasive content that makes DevOps teams feel excited, curious, and confident so that they can adopt & integrate industry best practices into their workflow.

## Messaging for Verticals
When to develop vertical messaging: The key is to determine if an industry has a certain pain point that another industry does not share.  You need to describe the problem (using industry specific terminology, if necessary) and also how your product solves these problems for them. Additionally, you can create high-level messaging and then branch off; for example if multiple industries are very security conscious, create security focused marketing, and adapt to select high value verticals.

## Checklist for good content<a name="checklist"></a>

- Relevant
- Useful
- User or customer-centered
- Clear, Consistent, Concise
- Supported

## Requesting content and copy reviews

1. If you are looking for content to include in an email, send to a customer, or share on social, check the [GitLab blog](/blog/) first.
1. If you need help finding relevant content to use, ask for help in the #content channel.
1. If the content you're looking for doesn't exist, open an issue in [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com) and label it `blog post`. The content team will evaluate if it's likely do well on the blog, in which case we will write the content. If the suggestion isn't likely do well, we will suggest you write it and we will help edit it.
1. If you are creating your own content and need a copy review, ping @erica. Please give at least 3 days notice.

## Writing blog posts

See the [blog handbook](/handbook/marketing/blog/) for detailed instructions on how to publish a blog post.

The Content Marketing team is responsible for increasing sessions on the GitLab blog month over month. We use data-drive insights to decide what to write on using the [content marketing dashboard](https://datastudio.google.com/u/0/reporting/1NIxCW309H19eLqc4rz0S8WqcmqPMK4Qb/page/1M). In order to hit our goals, we aim to publish at least 3 blog posts that will garner 10,000+ sessions.

### Trends

*From [2018 blog analysis](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/248) conducted in 2018-10

1. Average sessions per month on the blog: ~30,000
1. Average sessions per post in published month: 3,792
1. Average sessions per content marketing post in published month: 3,500
1. 55% of posts get <1,000 sessions in a month
1. 27.47% of posts hit our expected outcome of 1,000-4,999 sessions in published month
1. 28.57% of posts garner less than 499 sessions in published month
1. 9% of posts are "hits" (10K+ sessions in published month); "Hits" don't consistently perform well over time

**Breakdown by category of "hits":**

- Content: 37.5%
- Product: 31.35%
- Corporate: 25%

### Obeservations

1. There is not a strong correlation between # of sessions and topic
1. Strong performing content marketing posts focus on show and tell engineering stories
1. Posts really fall into the 5,000-9,999 session bracket (3.3%)
1. Content hack day posts tend to perform well (>1,000 sessoins in published month)

### Identifying and qualifying a high-performing blog post

**Qualifying story ideas:**

Look for the following patterns:

1. Team implementing a new techonology, process, or coding language
1. Deep dive into how a popular feature is made
1. Chronicling a performance improvement
1. Covering a controversial decision that was made

**Who and how to interview:**

1. Contact the technical subject matter expert. This can be someone who created the issue, or managed the project.
1. Set up a 30 minute interview and dig into:
  - What was the challenge?
  - What was the solution?
  - How did you go from point a to point b? Walk me through your thought process.
  - How was the solution implemented and what is a realistic use case of the solution?
  - What lessons were learned?
  - How did this make the GitLab product / development community better?
1. Optional: Contact the business subject matter expert. This could be a product manager or a product marketing manager.
1. Set up a 30 minute interview and dig into:
  - How does this solution help a user?
  - What business value does this solution bring?
  - How does this solution relate to our product?

### Attributes of a successful blog post:

1. Deep dive into a hard technical challenge.
1. Puts the reader in the shoes of the person who faced the challenge; reader learns via compelling example.
1. Intellectually satisfying; learning compontent.
1. Allows the reader to learn from someone else's mistake, and follows a problem/trials and triumphs/solution story arch.
1. Taking a controversial or unpopular stance on a topic, back by hard evidence.

### Examples of high-performing posts (20K+ sessions in published month):

1. [Meet the GitLab Web IDE](/2018/06/15/introducing-gitlab-s-integrated-development-environment/)
1. [How a fix in Go 1.9 sped up our Gitaly service by 30x](/2018/01/23/how-a-fix-in-go-19-sped-up-our-gitaly-service-by-30x/)
1. [Hey, data teams - We're working on a tool just for you](/2018/08/01/hey-data-teams-we-are-working-on-a-tool-just-for-you/index.html)
1. [Why we chose Vue.js](https://about.gitlab.com/2016/10/20/why-we-chose-vue/)
1. [How we do Vue: one year later](/2017/11/09/gitlab-vue-one-year-later/)

### Conducting a blog analysis

Pull information on all blog posts for document how many sessions each post received in the month, and how many sessions they received of all time. Categorize them by type, bracket, total sessions in month, total sessions to date, category, theme, and topic. Eventually add first touch point revenue data.
Search Google Drive for `Blog Performance` to find the appriopriate sheet to work from.

- Blog post links should be added as they are published and category, audience, theme, and topic should be filled out.
- The Managing Editor and Manager, Content Marketing should review last month on the 1st of the month to fill out session information and make observations
- Review 1st of each quarter to update total sessions

**Column explanations:**

- Type: helps identify the frequency of which certain types of information is shared on our current blog
- Bracket: helps quickly sort blog posts by performance level
- Category: indicates class of information
- Total sessions in month: how many sessions the post received in the month it was published
- Audience: indicates who we expect to reach
- Theme: indicates the structure of the post
- Topic: indicates the main subject covered

### Crediting blog posts

Add [a note](/handbook/product/technical-writing/markdown-guide/#note) at the end of a blog post reading "[Name] contributed to this story/post" if:

- You ghostwrote the post on behalf of someone else who is listed as the author
- You were heavily involved in editing/structuring/rewriting the post authored by someone else
- You wrote the post, but it is a company announcement listing GitLab as the author
- You wrote the post and are listed as the author, but wish to give credit to interviewees

## Key Responsibilities

At the highest level, Content Marketing is responsible for building awareness, trust, and preference for the GitLab brand with developers, engineers, and IT professionals.

1. Audience development
2. Editorial voice and style
3. Defining and executing the quarterly content strategy
4. Lead generation
