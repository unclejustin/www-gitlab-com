---
layout: markdown_page
title: "Spending Company Money - Equipment Examples"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Adapters and cables

#### USB adapters
  * TOTU 8-in-1 USB-C Hub - [US](http://a.co/d/8V80kOP)
  * FLYLAND Hub, 9-in-1 - [Germany](https://www.amazon.de/dp/B00OJY12BY/ref=cm_sw_r_tw_dp_U_x_lvgaCb2Y6M9YV)
  * VAVA USB-C Hub 8-in-1 Adapter - [Australia](https://www.amazon.com.au/dp/B07JCKCZGJ/ref=cm_sw_r_cp_ep)
  * UGREEN Ethernet to USB 3.0 Adapter - [US](http://a.co/d/1hO4hO4)
  * Yinboti USB-C Hub for New Macbook Pros - [US](https://www.amazon.com/gp/product/B07FMNJC6J/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)

#### Cables
  * AmazonBasics High-speed HDMI Cable - [US](http://a.co/d/acNQ9ij)

#### Combo Devices
  *  [USB-C Dock](http://www.caldigit.com/thunderbolt-3-dock/thunderbolt-station-3-plus/) ***Requires Manager Approval to expense***
    * Enables *stable* Dual Monitor Support for Engineers
      * Extended Desktop Support for DVI monitors requires ['active' displayport adaptors](http://www.cablematters.com/pc-33-33-cable-matters-gold-plated-displayport-to-dvi-male-to-female-adapter.aspx)
      * MacOS does not support [Multi-Stream Transport over DisplayPort](https://www.displayport.org/cables/driving-multiple-displays-from-a-single-displayport-output/)
    * Recharges Laptop over USB-C
    * Provides USB-A support for peripherals

### Notebook carrying bags
  * tomtoc 360° Protective Sleeve - [US](http://a.co/d/fGoBGYK)
  * NIDOO 15" - [Germany](https://www.amazon.de/dp/B072LVYC91/ref=cm_sw_r_tw_dp_U_x_eCgaCb15Q5S7Q)
  * Mosiso Sleeve - [Australia](https://www.amazon.com.au/dp/B01N0W1YIK/ref=cm_sw_r_cp_ep_dp_FDgaCb200161T)

### Monitors

#### Desktop monitors
  * ASUS VS248H-P 24" 1080p - [US](https://www.amazon.com/dp/B0058UUR6E/ref=cm_sw_r_tw_dp_U_x_2UgaCbCR87Z07)
  * ASUS PB277Q 27" 1440p - [US](https://www.amazon.com/gp/product/B01EN3Z7QQ/)
  * Acer S242HLDBID - [Germany](https://www.amazon.de/dp/B01AJTVCA8/ref=cm_sw_r_tw_dp_U_x_ETgaCb1YSFDB7)
  * SAMSUNG F350 23.6" 1080p - [Amazon](https://www.amazon.com.au/dp/B0771J3HXV/ref=cm_sw_r_cp_ep_dp_eTgaCbV4BCQP7)

#### Portable monitors
  * USB Touchscreen, 11.6" - [US](http://a.co/d/8pkwPSr)
  * Kenowa 15.6" - [Germany](https://www.amazon.de/dp/B07FZ5PNDV/ref=cm_sw_r_tw_dp_U_x_hRgaCb2K3A8BD)

### Headphones and earbuds
  * Mpow 059 Bluetooth Over Ear Headphones - [US](https://www.amazon.com/dp/B077XT82DD/ref=cm_sw_r_tw_dp_U_x_7VgaCbHHH1318)
  * JBL T450BT On-ear Bluetooth Headphones - [Germany](https://www.amazon.de/dp/B01M6WNWR6/ref=cm_sw_r_tw_dp_U_x_pXgaCb1RYXQK4)

### Office Furniture

#### Desks
  *  Autonomous SmartDesk 2 - Home Edition - [US and Europe](https://www.autonomous.ai/standing-desks/smart-desk-2-home)

#### Chairs
  * Hbada Ergonomic Office Chair - [US](https://www.amazon.com/dp/B01N0XPBB3/ref=cm_sw_r_tw_dp_U_x_73gaCbMT53PW5)
  * INTEY Ergonomic Office Chair - [Germany](https://www.amazon.de/dp/B0744GS6LR/ref=cm_sw_r_tw_dp_U_x_94gaCbG9F1CRB)
  * Kolina Ergonic Mesh Office Chair - [Australia](https://www.amazon.com.au/dp/B07BK7XDV8/ref=cm_sw_r_tw_dp_U_x_x6gaCbH91QM8K )

### Something else?
  *  No problem! Consider adding it to this list if others can benefit from it.
