---
layout: markdown_page
title: "SO.2.03 - Periodic Review of Physical Access Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SO.2.03 - Periodic Review of Physical Access

## Control Statement

GitLab performs physical access account reviews quarterly; corrective action is take where applicable.

## Context

This control refers to physical access of any GitLab facilities.

## Scope

This control is not applicable since GitLab has no facilities.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.2.03_periodic_review_of_physical_access.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.2.03_periodic_review_of_physical_access.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.2.03_periodic_review_of_physical_access.md).

## Framework Mapping

* ISO
  * A.11.1.2
* SOC2 CC
  * CC6.4
* PCI
  * 9.5
